// Declaração de classes.

class TodoList {
    constructor(){
        this.todos = [];
    }
} 

/// Map

const arr = [1,2,3,4,5]

const newArr = arr.map(function(item, index){
    return item + index;
})

/// Reduce
const soma = arr.reduce(function(total,item){
    return total + item;
})

console.log(soma);

/// Filter

const filtro = arr.filter(function(item){
    return item % 2 === 0;
})

console.log(filtro);

/// Find

const find = arr.find(function(item){
    return item === 10;
})

console.log(find)

/// REST 

function somatoria(...params){
    return params
}

somatoria(1,2,10,15,25,30);


/// SPREAD

const spreadArr = [1,2,3,4,5,6];

const spreadArr2 = [...spreadArr,7,8,9,10,11,12]

console.log(spreadArr2);


