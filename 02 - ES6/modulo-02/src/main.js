import api from "./api";

class App {
  constructor() {
    this.repositories = [];

    this.inputEl = document.querySelector("input[name=repository]");
    this.formEl = document.getElementById("repo-form");
    this.listEl = document.getElementById("repo-list");

    this.registerHandlers();
  }

  registerHandlers() {
    this.formEl.onsubmit = (event) => this.addRepository(event);
  }

  setLoading(loading = true) {
    if (loading === true) {
      let loadingEl = document.createElement("span");
      loadingEl.appendChild(document.createTextNode("Carregando"));
      loadingEl.setAttribute("id", "loading");

      this.formEl.appendChild(loadingEl);
    } else {
      document.getElementById("loading").remove();
    }
  }

  async addRepository(event) {
    event.preventDefault();

    const repoInput = this.inputEl.value;
    if (repoInput.length === 0) alert('Informe o repositório');

    this.setLoading();
    

    try {
      const response = await api.get(`/repos/${repoInput}`);

      const {
        name,
        description,
        html_url,
        owner: { avatar_url },
      } = response.data;

      this.repositories.push({
        name,
        description,
        avatar_url,
        html_url,
      });

      this.render();
    }catch(err){
        alert('Repositório informado não existe.')
    }
    this.setLoading(false);
  }

  render() {
    this.listEl.innerHTML = "";

    this.repositories.forEach((repo) => {
      let img = document.createElement("img");
      img.setAttribute("src", repo.avatar_url);

      let title = document.createElement("strong");
      title.appendChild(document.createTextNode(repo.name));

      let description = document.createElement("p");
      description.appendChild(document.createTextNode(repo.description));

      let link = document.createElement("a");
      link.setAttribute("target", "_blank");
      link.setAttribute("href", repo.html_url);
      link.appendChild(document.createTextNode("Acessar"));

      let li = document.createElement("li");
      li.appendChild(img);
      li.appendChild(title);
      li.appendChild(description);
      li.appendChild(link);

      this.listEl.appendChild(li);
    });
  }
}
new App();
