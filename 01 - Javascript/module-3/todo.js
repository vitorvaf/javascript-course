var inputElement = document.querySelector(".app input");
var buttonElement = document.querySelector(".app button");
var todos = document.querySelector(".app ul");

var listaTarefas = JSON.parse(localStorage.getItem("tarefas")) || [];

function carregarEventos() {
  buttonElement.onclick = adicionarElementos;
}

function carregarTarefas() {
   todos.innerHTML = ""; 

  for (tarefa of listaTarefas) {    
    var li = document.createElement("li");
    var texto = document.createTextNode(tarefa);
    var btnExcluir = document.createElement("a");
    var excluir = document.createTextNode("   Excluir");

    btnExcluir.setAttribute("href", "#");
    btnExcluir.appendChild(excluir);
    var index = listaTarefas.indexOf(tarefa);
    btnExcluir.setAttribute('onclick', 'deletarElementos('+ index +')')
    

    li.appendChild(texto);
    li.appendChild(btnExcluir);

    todos.appendChild(li);
  }

  localStorage.setItem("tarefas", JSON.stringify(listaTarefas));  
}

function adicionarElementos() {

  var novaTarefa = inputElement.value;
  listaTarefas.push(novaTarefa);
  
  inputElement.value = "";
  carregarTarefas();  
}

function deletarElementos(index){
    listaTarefas.splice(index, 1);
    carregarTarefas(); 
}

carregarTarefas();
carregarEventos();
